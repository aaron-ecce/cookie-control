<?php

namespace CookieControl;

use CookieControl\Policies\PolicyInterface;
use CookieControl\Parser\ParserInterface;
use CookieControl\CSP\Policy as ContentSecurity;
use CookieControl\CSP\Header;
use CookieControl\Cookie;

class Manager
{
    protected $registry;
    protected $parser;
    protected $policy;
    protected $csp;

    /**
     * Creates and registers the cookie manager
     *
     * @param  Registry             $registry Registry of all cookies and resources
     * @param  PolicyInterface      $policy   Policy instance for checking cookies against
     * @param  ParserInterface      $parser   The header parser for reading the cookie headers
     * @param  CSP\Policy           $csp      Content Security Policy to block third-party vendors
     * 
     * @return Manager              The newly created manager instance
     */
    public static function guard(Registry $registry, ?PolicyInterface $policy, ?ParserInterface $parser, ?ContentSecurity $csp)
    {
        // Create the manager instance
        $manager = new self($registry, $policy, $parser, $csp);

        // Register the manager on the headers callback
        header_register_callback($manager->createGuard());

        return $manager;
    }

    /**
     * Constructor
     * 
     * @param  PolicyInterface  $policy  Policy instance for checking cookies against
     * @param  ParserInterface  $parser  The header parser for reading the cookie headers
     * @param  CSP\Policy       $csp     Content Security Policy to block third-party vendors
     */
    public function __construct(Registry $registry, ?PolicyInterface $policy, ?ParserInterface $parser, ?ContentSecurity $csp)
    {
        $this->registry = $registry;
        $this->policy = $policy;
        $this->parser = $parser;
        $this->csp = $csp;
    }

    /**
     * Generate the callback handler before the headers are sent
     * 
     * @return function The callable function to run when headers are about to be output
     */
    public function createGuard()
    {
        // Autoload does not work on headers_register_callback, use constant to preload class
        Cookie::DIRECTIVE_SECURE;
        Header::CONTENT_POLICY();

        return function() {

            $cookies = [];
            $existing = [];

            // Attempt to retrieve all cookie headers
            foreach (headers_list() as $header) {

                // Try and get a cookie from the header
                if ($cookie = $this->parser->parse($header)) {
                    $cookies[$cookie['name']] = Cookie::fromState($cookie);
                }
            }

            // Loop through all cookie keys for existing cookies
            foreach ($_COOKIE as $name => $value) {
                $existing[$name] = Cookie::fromState(['name' => $name, 'value' => $value]);
            }

            // Unset all Set-Cookie headers
            header_remove('Set-Cookie');

            // Loop through all the cookies being created and make sure the policy allows it
            foreach ($cookies as $cookie) {
                // Check if the cookie is allowed
                if ($this->policy->allowed($cookie)) {
                    $cookie->save();
                }
            }

            // Loop through existing cookies and remove any that should not be there
            foreach ($existing as $cookie) {

                // Remove the cookie if it is not allowed
                if (!$this->policy->allowed($cookie)) {
                    $cookie->remove();
                }
            }

            // Loop through registered resources and check if they are allowed to be loaded
            foreach ($this->registry->getResources() as $name => $resource) {

                // Check if the resource is allowed to be loaded
                if ($this->policy->resourceAllowed($resource['resource'])) {
                    $resource['resource']->apply($this->csp);
                }
            }

            // Finally output the content security policy header
            header(Header::CONTENT_POLICY()->getValue() .': '. (string)$this->csp);
        };
    }
}
