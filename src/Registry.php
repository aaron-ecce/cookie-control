<?php

namespace CookieControl;

use CookieControl\Resources\ResourceInterface;

class Registry
{
	protected $cookies = array();
	protected $categories = array();
	protected $resources = array();
	protected $groups = array();

	/**
	 * Registers a cookie name to the registry
	 * 
	 * @param string $name     The name of the cookie
	 * @param string $category The category the cookie belongs to
	 * @param array  $meta     Extra information about the cookie, e.g. Third party, Purpose etc..
	 *
	 * @return $this
	 */
	public function add($name, $category, array $meta = array())
	{
		$this->cookies[$name] = [
			'category' => $category,
			'meta' => $meta
		];

		// Register the cookie name by category
		$this->categories[$category][] = $name;
		$this->categories[$category] = array_unique($this->categories[$category]);

		return $this;
	}

	/**
	 * Get cookie information from registry by name
	 * 
	 * @param  string     $name The name of the cookie to retrieve
	 * @return array|null       The cookie or null if not found
	 */
	public function get($name)
	{
		return $this->cookies[$name] ?? null;
	}

	/**
	 * Gets all cookies in the registry
	 * 
	 * @return array Cookies registered in the registry
	 */
	public function getCookies()
	{
		return $this->cookies;
	}

	/**
	 * Returns cookies by category
	 * 
	 * @param  string $category The name of the category to retrieve
	 * 
	 * @return array            The cookies registered to the category
	 */
	public function getCategory($category)
	{
		$cookies = [];

		if (isset($this->categories[$category])) {
			foreach ($this->categories[$category] as $name) {
				$cookies[] = $this->get($name);
			}
		}

		// Remove empty values and return
		return array_filter($cookies);
	}

	/**
	 * Adds a resource to the registry
	 * 
	 * @param Resource $resource A third-party resource to block with CSP
	 * @param string   $category Category to assign the resource to
	 * @param array    $meta     Optional meta data to assign to the resource
	 *
	 * @return  $this
	 */
	public function addResource(ResourceInterface $resource, $category, $meta = array())
	{
		$this->resources[$resource->getName()] = ['resource' => $resource, 'meta' => $meta, 'category' => $category];

		// Group the resources
		$this->groups[$category][] = $resource;
		return $this;
	}

	/**
	 * Retrieves a list of the registered resources
	 * 
	 * @return Resource The resource object
	 */
	public function getResources()
	{
		return $this->resources;
	}

	/**
	 * Loads the resources by category name
	 * 
	 * @param  string $category The category to retrieve the resources from
	 * 
	 * @return Resource[]       The resources in the category
	 */
	public function getResourceCategory($category)
	{
		$resources = [];

		if (isset($this->groups[$category])) {
			foreach ($this->groups[$category] as $resource) {
				$resources[] = $resource;
			}
		}

		// Remove empty values and return
		return array_filter($resources);
	}
}
