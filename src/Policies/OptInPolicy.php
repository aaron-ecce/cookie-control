<?php

namespace CookieControl\Policies;

use CookieControl\Cookie;
use CookieControl\Registry;
use CookieControl\Resources\ResourceInterface;

class OptInPolicy implements PolicyInterface
{
	/**
	 * Cookie registry with list of recorded and categorised cookies
	 * @var Registry
	 */
	protected $registry;

	/**
	 * Opted in categories
	 * @var array
	 */
	protected $categories = [];

	/**
	 * Constructor
	 * 
	 * @param Registry $registry   Cookie registry
	 * @param array    $categories Opted in categories
	 */
	public function __construct(Registry $registry, array $categories = [])
	{
		$this->registry = $registry;
		$this->categories = $categories;
	}

	/**
	 * Checks if the cookie is allowed to be set
	 * 
	 * @param  Cookie $cookie The cookie for validation
	 * 
	 * @return boolean        True if the cookie is allowed to be set
	 */
	public function allowed(Cookie $cookie)
	{
		$details = $this->registry->get($cookie->name);
		return !is_null($details) && in_array($details['category'], $this->categories);
	}

	/**
	 * Checks if the resource is allowed to be loaded
	 * 
	 * @param  Resource $resource The resource for validation
	 * 
	 * @return boolean            True if the resource is allowed to be loaded
	 */
	public function resourceAllowed(ResourceInterface $resource)
	{
		$resources = $this->registry->getResources();

		// Check if the resource has been registered
		if (isset($resources[$resource->getName()])) {
			
			// Check if the resource category is in the allowed category
			return in_array($resources[$resource->getName()]['category'], $this->categories);
		}

		return false;
	}
}
