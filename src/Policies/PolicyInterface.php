<?php

namespace CookieControl\Policies;

use CookieControl\Cookie;
use CookieControl\Resources\ResourceInterface;

interface PolicyInterface
{
	/**
	 * Checks if the cookie is allowed to be set
	 * 
	 * @param  Cookie $cookie The cookie for validation
	 * 
	 * @return boolean        True if the cookie is allowed to be set
	 */
	public function allowed(Cookie $cookie);

	/**
	 * Checks if the resource is allowed to be loaded
	 * 
	 * @param  Resource $resource The resource for validation
	 * 
	 * @return boolean            True if the resource is allowed to be loaded
	 */
	public function resourceAllowed(ResourceInterface $resource);
}
