<?php

namespace CookieControl\Resources;

abstract class AbstractResource implements ResourceInterface
{
	protected $name;

	/**
	 * Constructor for the resource
	 * 
	 * @param string $name The name of the resource
	 */
	public function __construct($name)
	{
		$this->name = $name;
	}

	/**
	 * Gets the name of the resource, e.g. google_analytics
	 * 
	 * @return string The name of the resource
	 */
	public function getName()
	{
		return $this->name;
	}
}
