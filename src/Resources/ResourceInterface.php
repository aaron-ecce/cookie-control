<?php

namespace CookieControl\Resources;

use CookieControl\CSP\Policy;

interface ResourceInterface
{
	/**
	 * Gets the name of the resource, e.g. google_analytics
	 * 
	 * @return string The name of the resource
	 */
	public function getName();

	/**
	 * Applies the domains for the resource to be allowed through the CSP
	 * 
	 * @param  Policy $policy The policy instance to register assets to
	 */
	public function apply(Policy $policy);
}