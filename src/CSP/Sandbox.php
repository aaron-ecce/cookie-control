<?php

namespace CookieControl\CSP;

use MyCLabs\Enum\Enum;

class Sandbox extends Enum
{
	private const ALLOW_DOWNLOADS_WITHOUT_USER = 'allow-downloads-without-user-activation';
	private const ALLOW_FORMS = 'allow-forms';
	private const ALLOW_MODALS = 'allow-modals';
	private const ALLOW_ORIENTATION_LOCK = 'allow-orientation-lock';
	private const ALLOW_POINTER_LOCK = 'allow-pointer-lock';
	private const ALLOW_POPUPS = 'allow-popups';
	private const ALLOW_POPUPS_ESCAPE = 'allow-popups-to-escape-sandbox';
	private const ALLOW_PRESENTATION = 'allow-presentation';
	private const ALLOW_SAME_ORIGIN = 'allow-same-origin';
	private const ALLOW_SCRIPTS = 'allow-scripts';
	private const ALLOW_STORAGE_ACCESS_USER_ACTIVATION = 'allow-storage-access-by-user-activation ';
	private const ALLOW_TOP_NAVIGATION = 'allow-top-navigation';
	private const ALLOW_TOP_NAVIGATION_USER_ACTIVATION = 'allow-top-navigation-by-user-activation';
}
