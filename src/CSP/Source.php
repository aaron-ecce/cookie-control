<?php

namespace CookieControl\CSP;

use MyCLabs\Enum\Enum;

class Source extends Enum
{
	private const ANY = '*';
	private const DATA = 'data:';
	private const HTTPS = 'https:';
	private const SELF = "'self'";
	private const UNSAFE_EVAL = "'unsafe-eval'";
	private const UNSAFE_HASHES = "'unsafe-hashes'";
	private const UNSAFE_INLINE = "'unsafe-inline'";
	private const NONE = "'none'";
	private const STRICT_DYNAMIC = "'strict-dynamic'";
	private const REPORT_SAMPLE = "'report-sample'";
}
