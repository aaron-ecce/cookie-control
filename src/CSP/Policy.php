<?php

namespace CookieControl\CSP;

use CookieControl\CSP\Directive;
use CookieControl\CSP\Source;
USE CookieControl\CSP\Header;

class Policy
{
	protected $directives = [];

	/**
	 * Policy constructor
	 */
	public function __construct()
	{
		// Map all the directives locally
		foreach (Directive::toArray() as $const => $value) {
			$this->directives[$value] = [];
		}

		// By default allow none
		$this->directives[Directive::DEFAULT_SRC()->getValue()] = [Source::NONE()->getValue() => true];
	}

	/**
	 * Adds a report url for any resources failing the CSP
	 * 
	 * @param string $uri The url to report the CSP violation to
	 */
	public function addReportUri($uri)
	{
		$this->addSource(Directive::REPORT_URI()->getValue(), $uri);
	}

	/**
	 * Adds a directive and value to the policy
	 * 
	 * @param Directive        $directive The directive we are setting a value for
	 * @param Source|string    $value     The value to apply to the directive
	 */
	public function addSource(Directive $directive, $value)
	{
		if ($value instanceof Source) {
			$value = $value->getValue();
		}

		// If the value is none, remove all existing valus for the directive
		if ($value == Source::NONE()) {
			$this->directives[$directive->getValue()] = [$value => true];
		} else {
			$this->directives[$directive->getValue()][$value] = true;
			unset($this->directives[$directive->getValue()][Source::NONE()->getValue()]);
		}

		return $this;
	}

	/**
	 * Convert the policy string ready for HTTP header
	 * 
	 * @return string Policy for a Http header
	 */
	public function __toString()
	{
		$directives = [];

		// Import each of the directives that are not empty
		foreach ($this->directives as $directive => $value) {
			if (!empty($value)) {
				$directives[] = $directive . ' ' . implode(' ', array_keys($value));
			}
		}

		return implode('; ', $directives);
	}
}
