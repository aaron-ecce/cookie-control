<?php

namespace CookieControl\CSP;

use MyCLabs\Enum\Enum;

class Header extends Enum
{
	private const CONTENT_POLICY = 'Content-Security-Policy';
	private const CONTENT_POLICY_REPORT = 'Content-Security-Policy-Report-Only';
}
