<?php

namespace CookieControl\CSP;

use MyCLabs\Enum\Enum;

class Directive extends Enum
{
	/**
	 * Fetch DIRECTIVES
	 */
	private const CHILD_SRC = 'child-src';
	private const CONNECT_SRC = 'connect-src';
	private const DEFAULT_SRC = 'default-src';
	private const FONT_SRC = 'font-src';
	private const FRAME_SRC = 'frame-src';
	private const IMG_SRC = 'img-src';
	private const MANIFEST_SRC = 'manifest-src';
	private const MEDIA_SRC = 'media-src';
	private const OBJECT_SRC = 'object-src';
	private const PREFECT_SRC = 'prefetch-src';
	private const SCRIPT_SRC = 'script-src';
	private const SCRIPT_SRC_ELEM = 'script-src-elem';
	private const SCRIPT_SRC_ATTR = 'script-src-attr';
	private const STYLE_SRC = 'style-src';
	private const STYLE_SRC_ELEM = 'style-src-elem';
	private const STYLE_SRC_ATTR = 'style-src-attr';
	private const WORKER_SRC = 'worker-src';

	/**
	 * Document Directives
	 */
	private const BASE_URI = 'base-uri';
	private const PLUGIN_TYPES = 'plugin-types';
	private const SANDBOX = 'sandbox';

	/**
	 * Navigation Directives
	 */
	private const FORM_ACTION = 'form-action';
	private const FRAME_ANCESTORS = 'frame-ancestors';
	private const NAVIGATE_TO = 'navigate-to';

	/**
	 * Reporting Directives
	 */
	private const REPORT_URI = 'report-uri';
	private const REPORT_TO = 'report-to';

	/**
	 * Other Directives
	 */
	private const BLOCK_MIXED_CONTENT = 'block-all-mixed-content';
	private const UPGRADE_INSECURE = 'upgrade-insecure-requests';
}
