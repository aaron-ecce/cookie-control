<?php

namespace CookieControl;

use DateTime;

class Cookie
{
	const DIRECTIVE_EXPIRES = 'Expires';
	const DIRECTIVE_MAXAGE = 'Max-Age';
	const DIRECTIVE_DOMAIN = 'Domain';
	const DIRECTIVE_PATH = 'Path';
	const DIRECTIVE_SECURE = 'Secure';
	const DIRECTIVE_HTTPONLY = 'HttpOnly';
	const DIRECTIVE_SAMESITE = 'SameSite';

	const SAMESITE_STRICT = 'Strict';
	const SAMESITE_LAX = 'Lax';
	const SAMESITE_NONE = 'None';

	const EXPIRES_DATE_FORMAT = 'D, d-M-Y H:i:s \G\M\T';

	/**
	 * Name of the cookie
	 * 
	 * @var string
	 */
	public $name;

	/**
	 * Value of the cookie
	 * 
	 * @var string
	 */
	public $value;

	/**
	 * Cookie expiry date
	 * 
	 * @var string
	 */
	public $Expires;

	/**
	 * Cookie path
	 * 
	 * @var string
	 */
	public $Path;

	/**
	 * Cookie Domain
	 * 
	 * @var string
	 */
	public $Domain;

	/**
	 * Cookie Max-Age - number of seconds until a cookie expires
	 * 
	 * @var string
	 */
	public $MaxAge;

	/**
	 * Cookie secure - Cookie will only appear on https://
	 * 
	 * @var boolean
	 */
	public $Secure = false;

	/**
	 * Cookie is HttpOnly - Forbids javascript from accessing the cookie
	 * 
	 * @var boolean
	 */
	public $HttpOnly = false;

	/**
	 * Cookie is Samesite - Asserts that cookie will not be sent with cross-origin requests
	 * 
	 * @var string
	 */
	public $SameSite = self::SAMESITE_NONE;

	/**
	 * Creates the cookie instance from a stored state
	 * 
	 * @param  array  $state The state to restore from
	 * 
	 * @return Cookie        A Cookie instance
	 */
	public static function fromState(array $state)
	{
		$cookie = new static;

		foreach ($state as $key => $value) {

			// Check for flag directives
			if (($key == self::DIRECTIVE_SAMESITE) || ($key == self::DIRECTIVE_HTTPONLY)) {
				$cookie->{$key} = true;
				continue;
			}

			// Normalise Max-Age directive
			if ($key == self::DIRECTIVE_MAXAGE) {
				$cookie->MaxAge = $value;
				continue;
			}

			// Match everything else
			$cookie->{$key} = $value;
		}

		return $cookie;
	}

	/**
	 * Checks if the cookie is already set in $_COOKIE
	 * 
	 * @return boolean True if the cookie has already been set
	 */
	public function exists()
	{
		return isset($_COOKIE[$this->name]);
	}

	/**
	 * Saves the cookie instance
	 * 
	 * @return void
	 */
	public function save()
	{
		// Before save, reformat expires date if it matches format
		$expires = $this->Expires;

		if (($date = DateTime::createFromFormat(self::EXPIRES_DATE_FORMAT, $this->Expires)) !== false) {
			$expires = $date->getTimestamp();
		}

		setcookie($this->name, $this->value, (int)$expires, $this->Path, $this->Domain, $this->Secure, $this->HttpOnly);
	}

	/**
	 * Unsets a cookie
	 */
	public function remove()
	{
		// Set the expire time to a past time
		setcookie($this->name, '', time() - 3600);

		if (isset($_COOKIE[$this->name])) {
			unset($_COOKIE[$this->name]);
		}
	}
}
