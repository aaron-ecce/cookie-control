<?php

namespace CookieControl\Parser;

use CookieControl\Cookie;

class CookieParser implements ParserInterface
{
	/**
	 * Parses a header string into a Cookie instance
	 * 
	 * @param  string $header The raw header string (Should start with Set-Cookie:)
	 * 
	 * @return Cookie         An instance of Cookie or null if the header is invalid
	 */
	public function parse($header)
	{
		// Make sure the header is valid
		if (!$this->verify($header)) {
			return null;
		}

		// Trim the header
		$header = trim(substr($header, 11));

		// Generate the cookie state
		$state = [];

		// Break the cookie into its raw components
		$segments = explode(';', $header);

		foreach ($segments as $segment) {

			$parts = $this->splitSegment($segment);
			$directive = $this->isDirective($parts['key']);

			// If we have a directive, set right away. Otherwise assume cookie name
			if ($directive) {
				$state[$directive] = $parts['value'];
			} else {
				$state['name'] = $parts['key'];
				$state['value'] = $parts['value'];
			}
		}

		return $state;
	}

	/**
	 * Splits a segment into a key, value pair
	 * 
	 * @param  string $segment The segment to split
	 * 
	 * @return array           The key of the segment with its value
	 */
	protected function splitSegment($segment)
	{
		$parts = explode('=', $segment);
		return [
			'key' => trim($parts[0]),
			'value' => $parts[1] ?? null
		];
	}

	/**
	 * Determines if the param is a valid directive
	 * 
	 * @param  string  $segment The directive to validate and get its type
	 * 
	 * @return boolean          The name of the directive or false if none matched
	 */
	protected function isDirective($directive)
	{

		switch (true) {
			case (strtolower(Cookie::DIRECTIVE_EXPIRES) == strtolower($directive)):
				return Cookie::DIRECTIVE_EXPIRES;
				break;

			case (strtolower(Cookie::DIRECTIVE_MAXAGE) == strtolower($directive)):
				return Cookie::DIRECTIVE_MAXAGE;
				break;

			case (strtolower(Cookie::DIRECTIVE_DOMAIN) == strtolower($directive)):
				return Cookie::DIRECTIVE_DOMAIN;
				break;

			case (strtolower(Cookie::DIRECTIVE_PATH) == strtolower($directive)):
				return Cookie::DIRECTIVE_PATH;
				break;

			case (strtolower(Cookie::DIRECTIVE_SECURE) == strtolower($directive)):
				return Cookie::DIRECTIVE_SECURE;
				break;

			case (strtolower(Cookie::DIRECTIVE_HTTPONLY) == strtolower($directive)):
				return Cookie::DIRECTIVE_HTTPONLY;
				break;

			case (strtolower(Cookie::DIRECTIVE_SAMESITE) == strtolower($directive)):
				return Cookie::DIRECTIVE_SAMESITE;
				break;
			
			default:
				return false;
				break;
		}
	}

	/**
	 * Determine if the parser will accept this header
	 * 
	 * @param  string  $header The raw header line
	 * 
	 * @return boolean         Returns true if the header is a Set-Cookie
	 */
	public function verify($header)
	{
		return (substr($header, 0, 10) == 'Set-Cookie');
	}
}
