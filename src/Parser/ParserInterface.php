<?php

namespace CookieControl\Parser;

interface ParserInterface
{
	/**
	 * Parses a header string
	 *
	 * @param  string $header The raw header to parse
	 *
	 * @return mixed          The result from parsing the header
	 */
	public function parse($header);

	/**
	 * Determine if the parser will accept this header
	 * 
	 * @param  string  $header The raw header line
	 * 
	 * @return boolean         Returns true if the header is valid for the parser
	 */
	public function verify($header);
}
