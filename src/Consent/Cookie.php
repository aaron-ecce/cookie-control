<?php

namespace CookieControl\Consent;

use CookieControl\Cookie as NormalCookie;
use Ramsey\Uuid\Uuid;
use Illuminate\Encryption\Encrypter;

/**
 * Cookie for storing user preferences and consent
 */
class Cookie extends NormalCookie
{
	/**
	 * The secret key for encryption
	 * 
	 * @var string
	 */
	protected $secret;

	/**
	 * The unique encrypted key of a unique ID and copy of consent
	 * 
	 * @var string
	 */
	protected $key;

	/**
	 * Unique ID generated for the consent cookie
	 * 
	 * @var string
	 */
	protected $uuid;

	/**
	 * Consent the user has provided
	 * 
	 * @var array
	 */
	protected $consent = [];

	/**
	 * Constructor
	 * 
	 * @param string $secret Secret key for encrypting consent to generate the key
	 * @param string $name   The name of the consent cookie
	 */
	public function __construct($secret = null, $name = null)
	{
		$this->secret = $secret;
		$this->name = $name ?? 'CookieConsent';
	}

	/**
	 * Sets the secret key for the encryption
	 * 
	 * @param string $secret Secret key for encryption
	 */
	public function setSecret($secret)
	{
		$this->secret = $secret;
	}

	/**
	 * Sets the full encrypted key for the cookie.
	 * 
	 * @param string $key The full encrypted key
	 */
	public function setKey($key)
	{
		$this->key = $key;
	}

	/**
	 * Gets the full encrypted key
	 * 
	 * @return string Full encrypted key of a unique id and consent the user has provided
	 */
	public function getKey()
	{
		return $this->key;
	}

	/**
	 * Sets the consent the user has provided
	 * 
	 * @param array $consent Consent the user has provided
	 */
	public function setConsent(array $consent)
	{
		$this->consent = $consent;
	}

	/**
	 * Gets the consent saved in the cookie
	 * 
	 * @return array Consent the user has provided
	 */
	public function getConsent()
	{
		return $this->consent;
	}

	/**
	 * Sets the uuid on the consent cookie
	 * 
	 * @param string $uuid The UUID for the cookie
	 */
	public function setUUID($uuid)
	{
		$this->uuid = $uuid;
	}

	/**
	 * Retrieves the uuid saved in the cookie
	 * 
	 * @return string The uuid
	 */
	public function getUUID()
	{
		return $this->uuid;
	}

	/**
	 * Update the cookie key and consent
	 * 
	 * @param  array  $consent The consent state
	 */
	public function updateConsent(array $consent)
	{
		$this->setConsent($consent);
		$this->setUUID(Uuid::uuid4());
		$this->setKey($this->generateKey($consent, $this->getUUID()));

		$this->value = base64_encode(json_encode([
			'key' => $this->key,
			'uuid' => $this->getUUID(),
			'consent' => $this->consent
		]));
	}

	/**
	 * Generates an encrypted key based on a random ID and the consent object
	 * 
	 * @param  array       $consent The consent state
	 * @param  string|null $uuid    Unique ID for the consent cookie
	 * 
	 * @return string          The encrypted key
	 */
	public function generateKey(array $consent, $uuid = null)
	{
		$uuid = is_null($uuid) ? Uuid::uuid4() : $uuid;
		$encrypter = new Encrypter($this->secret);

		return $encrypter->encrypt([
			'uuid' => $uuid,
			'consent' => $consent
		]);
	}

	/**
	 * Creates the cookie instance from a stored state
	 * 
	 * @param  array  $state The state to restore from
	 * 
	 * @return Cookie        A Cookie instance
	 */
	public static function fromState(array $state)
	{
		$cookie = parent::fromState($state);

		// If the cookie has a value, parse it and set the key and consent
		if (!empty($cookie->value)) {
			$state = json_decode(base64_decode(urldecode($cookie->value)), true);
			$cookie->setKey($state['key']);
			$cookie->setUUID(isset($state['uuid']) ? $state['uuid'] : Uuid::uuid4());
			$cookie->setConsent($state['consent']);
		}

		return $cookie;
	}
}
