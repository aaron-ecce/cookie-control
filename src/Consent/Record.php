<?php

namespace CookieControl\Consent;

use DateTime;
use geertw\IpAnonymizer\IpAnonymizer;

class Record
{
	/**
	 * An anonymized IP address of the user
	 * @var string
	 */
	protected $ip;

	/**
	 * Date and Time the record was created
	 * @var DateTime
	 */
	protected $date;

	/**
	 * The user agent of the browser the consent was recorded on
	 * @var string
	 */
	protected $userAgent;

	/**
	 * The url consent was provided on
	 * @var string
	 */
	protected $uri;

	/**
	 * A unique key and encrypted key to validate the user consent has not been modified
	 * @var string
	 */
	protected $key;

	/**
	 * A unique id for the consent provided.
	 * 
	 * @var string
	 */
	protected $uuid;

	/**
	 * The consent state the user has provided
	 * @var array
	 */
	protected $consent;

	/**
	 * Consent Record Constructor
	 * 
	 * @param string   $ip        An anonymized IP address of the user
	 * @param DateTime $date      Date and Time the record was created
	 * @param string   $userAgent The user agent of the browser the consent was recorded on
	 * @param string   $uri       The url consent was provided on
	 * @param string   $key       A unique key and encrypted key to validate the user consent has not been modified
	 * @param string   $uuid      Unique id for the consent
	 * @param array    $consent   The consent state the user has provided
	 */
	public function __construct($ip, DateTime $date, $userAgent, $uri, $key, $uuid, array $consent)
	{
		$this->ip = $ip;
		$this->date = $date;
		$this->userAgent = $userAgent;
		$this->uri = $uri;
		$this->key = $key;
		$this->uuid = $uuid;
		$this->consent = $consent;
	}

	/**
	 * Converts the record into an array
	 * 
	 * @return array The consent record as a simple array
	 */
	public function toArray()
	{
		return [
			'ip' => $this->ip,
			'date' => $this->date->format(DateTime::ISO8601),
			'ua' => $this->userAgent,
			'uri' => $this->uri,
			'key' => $this->key,
			'uuid' => $this->uuid,
			'consent' => json_encode($this->consent)
		];
	}

	/**
	 * Generates the record from a current request
	 * 
	 * @param  Cookie $consent The consent cookie instance for setting the consent state and key
	 * 
	 * @return Record          A new record instance for the current request
	 */
	public static function createFromGlobals(Cookie $consent)
	{
		return new self(
			IpAnonymizer::anonymizeIp($_SERVER['REMOTE_ADDR']),
			new DateTime,
			$_SERVER['HTTP_USER_AGENT'],
			self::getUrl(),
			$consent->getKey(),
			$consent->getUUID(),
			$consent->getConsent()
		);
	}

	/**
	 * Retrieves the current request URI
	 * 
	 * @return string Full url of the current request
	 */
	public static function getUrl()
	{
		// http(s)://domain.com
	 	$url = @$_SERVER['HTTPS'] == 'on' ? 'https://'. $_SERVER['SERVER_NAME'] : 'http://'.$_SERVER['SERVER_NAME'];

		if( @$_SERVER['SERVER_PORT'] != "80"  && @$_SERVER['SERVER_PORT'] != "443" ) {
			$url .=  ':'. @$_SERVER['SERVER_PORT'];
		}

		$url .= $_SERVER['REQUEST_URI'];
		
		return $url;
	}
}
