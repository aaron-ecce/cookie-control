# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]

## [0.4.2] - 2020-01-14

- Updated package from minimum stability

## [0.4.0] - 2020-01-14

- Added feature to control Content Security Policy and resources

## [0.3.1] - 2020-01-13

- Fixed uuid being undefined when created from old state

## [0.3.0] - 2020-01-13

- Added uuid to cookie and record for referencing consent

## [0.2.0] - 2020-01-10

- Added method getCookies on registry to get all registered cookies

## [0.1.1] - 2020-01-09

### Fixed

- Fixed issue with consent cookie not properly being decoded

## [0.1.0] - 2020-01-09

### Added

- Initial release of the library
