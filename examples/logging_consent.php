<?php

require __DIR__ . '/vendor/autoload.php';

/**
 * Once the user has given consent, you will want to store a log of when, where and what consent was given.
 * Logging consent will need to be done after each time the consent cookie gets updated.
 */
$cookie = CookieControl\Consent\Cookie::fromState([
	'secret' => 'secret-encryption-key',
	'value' => $_COOKIE['ConsentCookie']
]);

// Update the expires time and consent
$cookie->Expires = time()+60*60*24*30; // 30 days
$cookie->updateConsent([
	'essential' => true,
	'analytics' => true
]); // User has now opted into all categories
$cookie->save(); // Save the consent cookie into the users browser


/**
 * Once the consent cookie has been updates and saved, you will need to generate a consent record. This will
 * take the information from the current request and keep the neccessary information for logging. The Record
 * will need the consent cookie instance so it get gather the consent given and the encrypted value.
 *
 * A Record will contain the following details: anonymous ip address, date & time, browser user agent,
 * url of the request, encrypted key and finally the given consent.
 */
$record = CookieControl\Consent\Record::createFromGlobals($cookie);

/**
 * You can now store the Record with any storage mechanism you like, e.g. Log file
 */
file_put_contents('consent_log.log', implode("\t", $record->toArray()) . "\n", FILE_APPEND);
