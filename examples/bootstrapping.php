<?php

require __DIR__ . '/../vendor/autoload.php';


/**
 * Register list of cookies that is used on the website and assign
 * to category. Optionally you can add meta data to each
 * cookie, e.g. type = Third-Party etc.
 */
$registry = new CookieControl\Registry;

$registry
	->add('cookie1', 'essential', [
		'type' => 'Third-Party',
		'domain' => 'your.domain.com',
		'expires' => 'session',
		'purpose' => 'Description of the cookie',
		'other' => 'Any other meta information'
	])
	->add('__ga', 'analytics')
	->add('test-cookie', 'some-category', [
		'meta' => 'some metadata'
	]);


/**
 * Some sites will have a randomly generate name for the cookie, e.g. session ids.
 * This will need to be registered at website boot and assigned to the essential category
 * to make sure this cookie always gets loaded
 */
$registry->add(session_id(), 'essential', [
	'type' => 'First-Party',
	'domain' => 'your.domain.com',
	'expires' => 'session',
	'purpose' => 'Used for user authentication on the website.'
]);


/**
 * Websites will mostly use third-party services, e.g. Youtube, analytics etc.. Register those vendors here as well
 */
$registry->addResource(new CookieControl\Resources\GoogleRecaptcha, 'essential');

/**
 * Next, we will need to setup the Cookie policy. The policy will be in charge of
 * checking whether a cookie is allowed to be set based on the consent given by a user.
 *
 * Only one Policy has been created by default (OptInPolicy). This policy takes the Cookie Registry
 * and will check if the cookie is within the users selected categories.
 */
$policy = new CookieControl\Policies\OptInPolicy($registry, ['essential']); // This will only allow cookies within the "essential" category
$policy = new CookieControl\Policies\OptInPolicy($registry, []); // This will prevent any first party cookie from being set on the site.


/**
 * Prepare a content securty policy to block third-party resources, e.g. Youtube. These resources
 * when requested to their domain can load in cookies. By controlling this content, you can prevent
 * the resources from bringing extra cookies.
 */
$csp = new CookieControl\CSP\Policy;

/**
 * Once the Cookie Policy has been defined, this will be passed to the "Manager". The Manager handles registration for the header
 * callback (to prevent cookies from being sent to the browser) and filters the cookies based on the Policy provided.
 */
$manager = new CookieControl\Manager($registry, $policy, new CookieControl\Parser\CookieParser, $csp);
header_register_callback($manager->createGuard()); // createGuard returns a callable to do the actual filtering

// This is a shortcut for the above
$manager = CookieControl\Manager::guard($registry, $policy, new CookieControl\Parser\CookieParser, $csp);
