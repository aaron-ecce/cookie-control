<?php

require __DIR__ . '/vendor/autoload.php';

/**
 * For the Cookie Control, user consent will need to be store in a cookie to
 * keep track of the cookies the user has opted in for. A dedicated cookie  object
 * has been setup for this. The Consent Cookie will require a secret key for Encryption,
 * a UUID and copy of consent will be encrypted. This can be referenced later and can also
 * be used to validate the stored consent has not been modified.
 */
$cookie = new CookieControl\Consent\Cookie('secret-encryption-key'); // Default cookie name is CookieConsent
$cookie = new CookieControl\Consent\Cookie('secret-encryption-key', 'custom_consent_cookie'); // Renamed consent cookie to 'custom_consent_cookie'


/**
 * Below is an example of creating/updating the consent cookie. The cookie is saved for 30 days with 
 * the consent the user has provided (opted into the essential category but not analytics). Calling the
 * "updateConsent" method will generate a brand new UUID and set the updated consent then encrypt.
 *
 * key = encrypt(UUID + consent);
 */
$cookie->Expiries = time()+60*60*24*30; // 30 days
$cookie->updateConsent([
	'essential' => true,
	'analytics' => false
]);
$cookie->save();


/**
 * To check if the consent cookie exists, just call the below. This is just a wrapper to checking the $_COOKIE global.
 */
$cookie->exists(); // True if the cookie has already been saveed in the browser.


/**
 * To retrieve already saved consent cookie, you can load from the cookie state
 */
$cookie = CookieControl\Consent\Cookie::fromState([
	'secret' => 'secret-encryption-key',
	'value' => $_COOKIE['CookieConsent'] // Name of your consent cookie.
]);
