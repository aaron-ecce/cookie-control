# Cookie Control

Management of cookies for the [cookie law](https://ico.org.uk/for-organisations/guide-to-pecr/cookies-and-similar-technologies/)

## Requirements

 * PHP 7.1+

## Installation

```composer require aaron-ecce/cookie-control```

## Usage

### Register list of cookies

To get started, you will need to setup a list of cookies that are used on your site and assign metadata.


```php
<?php
$registry = new CookieControl\Registry;
$registry->add('cookie_name', 'category', ['metadata' => 'example']);
...
```

**Make sure to register the consent cookie to the registry. You will need to make sure this is categorised as a part of your sites essential cookies**

### Register resources

Most sites will use a third-party service of some sort, e.g. Youtube, Google Analytics etc. These services will load cookies which are out of your control. By adding a resource, you can control when these
services are loaded via a content security policy.

```php
<?php
$registry->addResource(new CookieControl\Resources\GoogleRecaptcha, 'category', ['metadata' => 'example']);
...
```

**You will need to register all third-party services on a website as the default policy is to block by default. If you would like a resource to always be loaded, make sure to add to your essential category.**

### Creating a Cookie Policy

You will need to setup a cookie policy. The policy will basically determine if a cookie is valid for the provided policy. A simple OptInPolicy is included to allow a user to opt into a specified category.


```php
<?php
$policy = new CookieControl\Policies\OptInPolicy($registry, ['essential']);
```

The above example policy allows only "essential" cookies to be set in the users browser.

### Register the Cookie Manager

The cookie manager will register itself to watch any cookie headers about to be set and filter out any cookies that are not apart of the policy.


```php
<?php
CookieControl\Manager::guard($registry, $policy, new CookieControl\Parser\CookieParser);
```

The manager will need to be registered before any headers are sent from the request. Preferably set this as early as you can in your bootstrap file.

### Consent Cookie

The consent cookie will store the consent provided by the user and an encrypted key to validate that consent has not been modified.

#### Creating the consent cookie

When the user has opted into their chosen categories, you will need to save their consent into a cookie to later feed into the policy for filtering.


```php
<?php
$consent = new CookieControl\Consent\Cookie('secret-encryption-key');
$consent->Expires = time()+60*60*24*30; // 30 days
$consent->updateConsent(['category_name' => true, 'other_category' => false]);
$consent->save();
```

#### Reading the consent cookie

You will need to be able to load the existing consent of the user to apply to your policy to filter any opted out cookies.


```php
<?php
$consent = CookieControl\Consent\Cookie::fromState([
    'secret' => 'secret-encryption-key',
    'value' => $_COOKIE['CookieConsent']
]);
```

#### Updating with new consent

Load the existing consent cookie and set the new provided consent. When saved, the cookie will regenerate a new encrypted key and a copy of the new consent. Remember to set the cookie expiry time.


```php
<?php
$consent = CookieControl\Consent\Cookie::fromState([
 	'secret' => 'secret-encryption-key',
 	'value' => $_COOKIE['CookieConsent']
]);
$consent->Expires = time()+60*60*24*30; // 30 days
$consent->updateConsent(['category_name' => true, ...]);
$consent->save();
```

### Logging Consent

Once consent has been provided or updated, you will need to log when, where and what consent was given. You can generate a consent record which can then be used in a storage mechanism of your choice. The record will need the consent cookie instance for the encrypted key and the stored consent.


```php
<?php
$record = CookieControl\Consent\Record::createFromGlobals($consent);
```

This record can then be later stored into a log file, e.g:


```php
<?php
file_put_contents('consent.log', implode("\t", $record->toArray()) . "\n", FILE_APPEND);
```

The record will hold the necessary information about the given consent but the storage mechanism is outside the scope of this library.

### References

* [Cookies and similar technologies](https://ico.org.uk/for-organisations/guide-to-pecr/cookies-and-similar-technologies/)
* [MDN: Set-Cookie](https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers/Set-Cookie)
